//
//  connection.cpp
//  lib_http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "connection.hpp"
#include <utility>
#include <vector>
#include "connection_manager.hpp"
#include "irequest_handler.hpp"

namespace http {
    namespace server {
        
        connection::connection(boost::asio::ip::tcp::socket socket,
                               connection_manager& manager,
                               std::shared_ptr<irequest_handler> handler)
        : socket_(std::move(socket)),
        connection_manager_(manager),
        request_handler_(handler)
        {
        }
        
        void connection::start()
        {
            do_read();
        }
        
        void connection::stop()
        {
            socket_.close();
        }
        
        void connection::do_read()
        {
            auto self(shared_from_this());
            socket_.async_read_some(boost::asio::buffer(buffer_),
                                    [this, self](boost::system::error_code ec, std::size_t bytes_transferred)
                                    {
                                        if (!ec)
                                        {
                                            request_parser::result_type result;
                                            std::tie(result, std::ignore) = request_parser_.parse(
                                                                                                  request_, buffer_.data(), buffer_.data() + bytes_transferred);
                                            
                                            if (result == request_parser::good)
                                            {
                                                reply thereply = request_handler_->handle_request(request_);
                                                do_write(thereply);
                                            }
                                            else if (result == request_parser::bad)
                                            {
                                                reply thereply = reply::stock_reply(reply::bad_request);
                                                do_write(thereply);
                                            }
                                            else
                                            {
                                                do_read();
                                            }
                                        }
                                        else if (ec != boost::asio::error::operation_aborted)
                                        {
                                            connection_manager_.stop(shared_from_this());
                                        }
                                    });
        }
        
        void connection::do_write(const reply& thereply)
        {
            auto self(shared_from_this());
            boost::asio::async_write(socket_, thereply.to_buffers(),
                                     [this, self](boost::system::error_code ec, std::size_t)
                                     {
                                         if (!ec)
                                         {
                                             // Initiate graceful connection closure.
                                             boost::system::error_code ignored_ec;
                                             socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both,
                                                              ignored_ec);
                                         }
                                         
                                         if (ec != boost::asio::error::operation_aborted)
                                         {
                                             connection_manager_.stop(shared_from_this());
                                         }
                                     });
        }
        
    } // namespace server
} // namespace http