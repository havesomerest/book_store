//
// server.hpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER_HPP
#define HTTP_SERVER_HPP

#include <boost/asio.hpp>
#include <string>
#include "connection.hpp"
#include "connection_manager.hpp"
#include "irequest_handler.hpp"
#include "request_handler.hpp"

namespace http {
    namespace server {
        
        /// The top-level class of the HTTP server.
        class server
        {
        public:
            server(const server&) = delete;
            server& operator=(const server&) = delete;
            
            /// Construct the server to listen on the specified TCP address and port, and
            /// serve up files from the given directory.
            explicit server(const std::string& address, const std::string& port,
                            const std::string& doc_root);
            
            
            explicit server(const std::string& address, const std::string& port,
                            const std::string& doc_root, std::shared_ptr<irequest_handler> request_handler);
            
            /// Run the server's io_service loop.
            void run();
            
        private:
            class impl;
            std::shared_ptr<impl> impl_;
        };
        
    } // namespace server
} // namespace http

#endif // HTTP_SERVER_HPP