//
//  irequest_handler.hpp
//  lib_http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef HTTP_IREQUEST_HANDLER_HPP
#define HTTP_IREQUEST_HANDLER_HPP

#include "reply.hpp"
#include "request.hpp"

namespace http
{
    namespace server
    {
        /*
            Plugin interface for handling incoming http requests.
        */
        class irequest_handler
        {
        public:
            virtual reply handle_request(const request& req) = 0;
        };
    }
}

#endif
