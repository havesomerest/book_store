//
//  book_entry.hpp
//  lib_book_store
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef book_entry_hpp
#define book_entry_hpp

#include <string>
#include <vector>

namespace book_store
{
    struct book_entry
    {
        std::string title;
        std::string author;
        std::string description;
        std::string price;
        std::string isbn;
        std::string publisher_email;
        std::string publisher;
        
        static std::string to_json(const book_entry& entry);
        static std::string to_json(const std::vector<book_entry>& entries);
    };
}

#endif /* book_entry_hpp */
