//
//  mock_db_gateway.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "mock_db_gateway.hpp"
#include <fstream>
#include <map>
#include "lib_book_store/book_entry.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace bookstore
{
    namespace server
    {
        class mock_db_gateway::impl
        {
            std::map<std::string, book_store::book_entry> db_;
        public:
            impl()
            {
                using boost::property_tree::ptree;
                ptree pt;
                try
                {
                    /*
                        Read database from text file, serialize to boost::ptree
                        then build map of (isbn, book_entry) pairs.
                    */
                    read_json("books_db.json", pt);
                    
                    for (auto&& book_object : pt)
                    {
                        book_store::book_entry entry {};
                        for (auto&& node : book_object.second)
                        {
                            // since items can appear in random order we should
                            // check for key type
                            std::string name = node.first;
                            if (name == "title")
                                entry.title = node.second.data();
                            else if (name == "author")
                                entry.author = node.second.data();
                            else if (name == "description")
                                entry.description = node.second.data();
                            else if (name == "price")
                                entry.price = node.second.data();
                            else if (name == "isbn")
                                entry.isbn = node.second.data();
                            else if (name == "publisher_email")
                                entry.publisher_email = node.second.data();
                            else if (name == "publisher")
                                entry.publisher = node.second.data();
                        }
                        
                        if (!entry.isbn.empty())
                            db_[entry.isbn] = entry;
                    }
                }
                catch(const std::exception& e)
                {
                    std::cout << "mock_db_gateway: exception " << e.what() << std::endl;
                }
            }
            
            std::vector<book_store::book_entry> find(const std::string& query)
            {
                /*
                    This part can be imporoved to break query into tokens and
                    test each word separately
                */
                auto contains = [](const std::string& src, const std::string& f)
                {
                    auto it = std::search(
                                          src.begin(), src.end(),
                                          f.begin(),   f.end(),
                                          [](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
                                          );
                    return (it != src.end() );
                };
                
                std::vector<book_store::book_entry> results {};
                
                for (auto&& db_entry : db_)
                {
                    bool has_match = contains(db_entry.second.title, query) ||
                                     contains(db_entry.second.author, query) ||
                                     contains(db_entry.second.description, query);
                    if (has_match)
                        results.emplace_back(db_entry.second);
                }
                
                return results;
            }
            
            std::tuple<bool, book_store::book_entry> get(const std::string& isbn)
            {
                auto it = std::find_if(db_.begin(), db_.end(), [isbn](decltype(*db_.begin()) entry)
                {
                    return entry.second.isbn == isbn;
                });
                if (it == db_.end())
                    return std::make_tuple(false, book_store::book_entry{});
                else
                    return std::make_tuple(true, it->second);
            }
        };
        
        mock_db_gateway::mock_db_gateway()
        {
            impl_ = std::make_shared<impl>();
        }
        
        std::vector<book_store::book_entry> mock_db_gateway::find(const std::string& query)
        {
            return impl_->find(query);
        }
        
        std::tuple<bool, book_store::book_entry> mock_db_gateway::get(const std::string& isbn)
        {
            return impl_->get(isbn);
        }
    }
}