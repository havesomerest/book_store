//
//  book_entry.cpp
//  lib_book_store
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "book_entry.hpp"

namespace book_store
{
    std::string book_entry::to_json(const book_entry& entry)
    {
        std::string author = "\"author\":\"" + entry.author + "\",";
        std::string description = "\"description\":\"" + entry.description + "\",";
        std::string price = "\"price\":\"" + entry.price + "\",";
        std::string isbn = "\"isbn\":\"" + entry.isbn + "\",";
        std::string publisher_email = "\"publisher_email\":\"" + entry.publisher_email + "\",";
        std::string publisher = "\"publisher\":\"" + entry.publisher + "\",";
        std::string title = "\"title\":\"" + entry.title + "\"";
        return "{" + publisher_email + publisher + isbn + price + description + author + title + "}";
    }
    
    std::string book_entry::to_json(const std::vector<book_entry>& entries)
    {
        std::string result;
        for (auto&& be : entries)
        {
            if (!result.empty())
                result += ",";
            result += book_store::book_entry::to_json(be);
        }
        return result;
    }
}