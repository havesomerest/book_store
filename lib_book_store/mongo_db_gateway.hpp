//
//  mongo_db_gateway.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef mongo_db_gateway_hpp
#define mongo_db_gateway_hpp

#include "lib_book_store/idb_gateway.hpp"

namespace bookstore
{
    namespace server
    {
        /*
            MongoDB gateway implementation
        */
        class mongo_db_gateway : public idb_gateway
        {
        public:
            std::vector<book_store::book_entry> find(const std::string& query) override;
        };
    }
}


#endif /* mongo_db_gateway_hpp */
