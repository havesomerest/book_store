//
//  mock_db_gateway.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef mock_db_gateway_hpp
#define mock_db_gateway_hpp

#include "lib_book_store/idb_gateway.hpp"

namespace bookstore
{
    namespace server
    {
        /*
            Plain text file database
        */
        class mock_db_gateway : public idb_gateway
        {
        public:
            mock_db_gateway();
            
            std::vector<book_store::book_entry> find(const std::string& query) override;
            
            std::tuple<bool, book_store::book_entry> get(const std::string& isbn) override;
            
        private:
            class impl;
            std::shared_ptr<impl> impl_;
        };
    }
}


#endif /* mock_db_gateway.hpp */
