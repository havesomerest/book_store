//
//  idb_gateway.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef idb_gateway_h
#define idb_gateway_h

#include <vector>
#include <string>
#include "lib_book_store/book_entry.hpp"

namespace bookstore
{
    namespace server
    {  
        /*
            Database gateway class, incapsulates all db related ativity
        */
        class idb_gateway
        {
        public:
            virtual std::vector<book_store::book_entry> find(const std::string& query) = 0;
            
            virtual std::tuple<bool, book_store::book_entry> get(const std::string& isbn) = 0;
        };
    }
}

#endif /* idb_gateway_h */
