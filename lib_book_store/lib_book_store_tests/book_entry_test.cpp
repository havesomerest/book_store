//
//  book_entry_test.cpp
//  lib_book_store
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include <third_party/catch/catch.hpp>
#include "lib_book_store/book_entry.hpp"

using namespace book_store;

static std::string create_serialized_book_entry()
{
    return "{\"publisher_email\":\"theemail\",\"publisher\":\"thepublisher\",\"isbn\":\"theisbn\",\"price\":\"theprice\",\"description\":\"thedescription\",\"author\":\"theauthor\",\"title\":\"thetitle\"}";
}

static std::string create_serialized_book_entries()
{
    return "{\"publisher_email\":\"theemail\",\"publisher\":\"thepublisher\",\"isbn\":\"theisbn\",\"price\":\"theprice\",\"description\":\"thedescription\",\"author\":\"theauthor\",\"title\":\"thetitle\"},{\"publisher_email\":\"theemail\",\"publisher\":\"thepublisher\",\"isbn\":\"theisbn\",\"price\":\"theprice\",\"description\":\"thedescription\",\"author\":\"theauthor\",\"title\":\"thetitle\"},{\"publisher_email\":\"theemail\",\"publisher\":\"thepublisher\",\"isbn\":\"theisbn\",\"price\":\"theprice\",\"description\":\"thedescription\",\"author\":\"theauthor\",\"title\":\"thetitle\"}";
}

SCENARIO("json conversion", "[book_entry]")
{
    WHEN("converting some book_entry into json")
    {
        book_entry entry = { "thetitle",
                             "theauthor",
                             "thedescription",
                             "theprice",
                             "theisbn",
                             "theemail",
                             "thepublisher"
                            };
        std::string result = book_entry::to_json(entry);
        REQUIRE(result == create_serialized_book_entry());
    }
    
    WHEN("converting vector of book_entry into json")
    {
        book_entry entry = { "thetitle",
            "theauthor",
            "thedescription",
            "theprice",
            "theisbn",
            "theemail",
            "thepublisher"
        };
        std::vector<book_entry> entries{entry, entry, entry};
        std::string result = book_entry::to_json(entries);
        REQUIRE(result == create_serialized_book_entries());
    }
}
