//
//  html_generator.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef html_generator_hpp
#define html_generator_hpp

#include <string>

namespace bookstore
{
    namespace server
    {
        class html_generator
        {
        public:
            static std::string search_page_content();
            
            static std::string admin_search_page_content();
            
            static std::string book_search_query_content(const std::string& json_data);
            
            static std::string book_info_content(const std::string& json_data);
            
            static std::string admin_book_info_content(const std::string& json_data);
            
            static std::string book_order_content(const std::string& json_data);
            
            static std::string book_add_content();
            
            static std::string book_edit_content(const std::string& json_data);
        };
    }
}

#endif /* html_generator_hpp */
