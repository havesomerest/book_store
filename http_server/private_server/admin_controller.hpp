//
//  admin_controller.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef admin_controller_hpp
#define admin_controller_hpp

#include "iclient_controller.hpp"

namespace bookstore
{
    namespace server
    {
        class idb_gateway;
        
        /*
            admin_controller provides priveleged access to the system, enhaced interface
            and ability to make side-effects, like adding new books into the database
        */
        class admin_controller : public iclient_controller
        {
        public:
            admin_controller(std::shared_ptr<idb_gateway> db_gw);
            
            virtual http::server::reply on_search_page_request() override;
            
            virtual http::server::reply on_book_search_query(const std::string& query) override;
            
            virtual http::server::reply on_book_info(const std::string& isbn) override;
            
            virtual http::server::reply on_book_order(const std::string& isbn) override;
            
            virtual http::server::reply on_book_add() override;
            
            virtual http::server::reply on_book_edit(const std::string& isbn) override;
            
        private:
            class impl;
            std::shared_ptr<impl> impl_;
        };
    }
}

#endif /* admin_controller_hpp */
