//
//  html_generator.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "html_generator.hpp"
#include <fstream>

namespace bookstore
{
    namespace server
    {
        static std::string json_placeholder = "%JSON_DATA%";
        
        bool replace(std::string& str, const std::string& from, const std::string& to) {
            size_t start_pos = str.find(from);
            if(start_pos == std::string::npos)
                return false;
            str.replace(start_pos, from.length(), to);
            return true;
        }
        
        std::string read_from_file(const std::string& filename)
        {
            std::ifstream file(filename);
            std::string file_content((std::istreambuf_iterator<char>(file)),
                                     std::istreambuf_iterator<char>());
            return file_content;
        }
        
        std::string html_generator::search_page_content()
        {
            return read_from_file("search_page.html");
        }
        
        std::string html_generator::admin_search_page_content()
        {
            return read_from_file("admin_search_page.html");
        }
        
        std::string html_generator::book_search_query_content(const std::string& json_data)
        {
            std::string file_content = read_from_file("book_search_results.html");
            std::string substitute("{\"books\":[" + json_data + "]}");
            replace(file_content, json_placeholder, substitute);
            return file_content;
        }
        
        std::string html_generator::book_info_content(const std::string& json_data)
        {
            std::string file_content = read_from_file("book_info.html");
            replace(file_content, json_placeholder, json_data);
            return file_content;
        }
        
        std::string html_generator::admin_book_info_content(const std::string& json_data)
        {
            std::string file_content = read_from_file("admin_book_info.html");
            replace(file_content, json_placeholder, json_data);
            return file_content;
        }
        
        std::string html_generator::book_order_content(const std::string& json_data)
        {
            std::string file_content = read_from_file("book_order.html");
            replace(file_content, json_placeholder, json_data);
            return file_content;
        }
        
        std::string html_generator::book_add_content()
        {
            return read_from_file("add_book_page.html");
        }
        
        std::string html_generator::book_edit_content(const std::string& json_data)
        {
            std::string file_content = read_from_file("edit_book_page.html");
            replace(file_content, json_placeholder, json_data);
            return file_content;
        }
    }
}