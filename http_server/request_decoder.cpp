//
//  request_decoder.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "request_decoder.hpp"
#include <map>

namespace bookstore
{
    namespace server
    {
        static const std::string search_query_literal = "/?search_query=";
        static const std::string isbn_literal = "/?isbn=";
        static const std::string order_isbn_literal = "/?order=";
        static const std::string add_book_literal = "/add_book";
        static const std::string edit_book_literal = "/?edit_book=";
        
        void str_replace(std::string& str, const std::string& from, const std::string& to) {
            size_t start_pos = str.find(from);
            while (start_pos != std::string::npos)
            {
                str.replace(start_pos, from.length(), to);
                start_pos = str.find(from);
            }
        }
        
        std::string extract_literal(const std::string& from, const std::string& literal)
        {
            std::size_t pos = from.find(literal);
            if (pos != std::string::npos)
            {
                std::string extracted(from.begin() + pos + literal.size(), from.end());
                str_replace(extracted, "+", " ");
                return extracted;
            }
            else
                return from;
        }
        
        client_request_type request_decoder::decode(const http::server::request& req)
        {
            std::string uri = req.uri;
            if (uri == "/")
                return client_request_type::get_search_page;
            else if (uri.find(search_query_literal) != std::string::npos)
                return client_request_type::get_books_query;
            else if (uri.find(isbn_literal) != std::string::npos)
                return client_request_type::get_book_info;
            else if (uri.find(order_isbn_literal) != std::string::npos)
                return client_request_type::order_book;
            else if (uri.find(add_book_literal) != std::string::npos)
                return client_request_type::add_book;
            else if (uri.find(edit_book_literal) != std::string::npos)
                return client_request_type::edit_book;
            else
                return client_request_type::unknown;
        }
        
        std::string request_decoder::extract_query(const std::string &query)
        {
            return extract_literal(query, search_query_literal);
        }
        
        std::string request_decoder::extract_isbn(const std::string& query)
        {
            return extract_literal(query, isbn_literal);
        }
        
        std::string request_decoder::extract_order_isbn(const std::string& query)
        {
            return extract_literal(query, order_isbn_literal);
        }
        
        std::string request_decoder::extract_edit_isbn(const std::string& query)
        {
            return extract_literal(query, edit_book_literal);
        }
    }
}