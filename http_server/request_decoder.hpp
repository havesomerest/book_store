//
//  request_decoder.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef request_decoder_hpp
#define request_decoder_hpp

#include "lib_http_server/request.hpp"

namespace bookstore
{
    namespace server
    {
        enum class client_request_type
        {
            get_search_page,
            get_books_query,
            get_book_info,
            order_book,
            add_book,
            edit_book,
            unknown
        };
        
        /*
            request_decoder maps http requests to application specific actions.
            Provides helper functions for http request parsing.
        */
        class request_decoder
        {
        public:
            static client_request_type decode(const http::server::request& req);
            
            static std::string extract_query(const std::string& query);
            
            static std::string extract_isbn(const std::string& query);
            
            static std::string extract_order_isbn(const std::string& query);
            
            static std::string extract_edit_isbn(const std::string& query);
        };
    }
}

#endif /* request_decoder_hpp */
