//
//  request_handler.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "request_handler.hpp"
#include "iclient_controller.hpp"
#include "request_decoder.hpp"
#include <iostream>

namespace bookstore
{
    namespace server
    {
        class request_handler::impl
        {
            std::shared_ptr<iclient_controller> client_controller_;
            
        public:
            impl(std::shared_ptr<iclient_controller> client_controller)
            {
                client_controller_ = client_controller;
            }
            
            http::server::reply handle_request(const http::server::request& req)
            {
                if (client_controller_)
                {
                    client_request_type rtype = request_decoder::decode(req);
                    switch (rtype)
                    {
                        case client_request_type::get_search_page:
                            return client_controller_->on_search_page_request();
                        case client_request_type::get_books_query:
                        {
                            std::string search_query = request_decoder::extract_query(req.uri);
                            return client_controller_->on_book_search_query(search_query);
                        }
                        case client_request_type::get_book_info:
                        {
                            std::string isbn = request_decoder::extract_isbn(req.uri);
                            return client_controller_->on_book_info(isbn);
                        }
                        case client_request_type::order_book:
                        {
                            std::string isbn = request_decoder::extract_order_isbn(req.uri);
                            return client_controller_->on_book_order(isbn);
                        }
                        case client_request_type::add_book:
                        {
                            return client_controller_->on_book_add();
                        }
                        case client_request_type::edit_book:
                        {
                            std::string isbn = request_decoder::extract_edit_isbn(req.uri);
                            return client_controller_->on_book_edit(isbn);
                        }
                        default:
                            break;
                    }
                    return http::server::reply::stock_reply(http::server::reply::not_found);
                }
                else
                {
                    std::cout << "request_handler: error, client controller is not set." << std::endl;
                    http::server::reply reply;
                    return reply;
                }
            }
        };
        
        request_handler::request_handler(std::shared_ptr<iclient_controller> client_controller)
        {
            impl_ = std::make_shared<impl>(client_controller);
        }
        
        http::server::reply request_handler::handle_request(const http::server::request& req)
        {
            return impl_->handle_request(req);
        }
    }
}
