//
//  iclient_controller.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef iclient_controller_hpp
#define iclient_controller_hpp

#include "lib_http_server/reply.hpp"

namespace bookstore {
    namespace server
    {
        /*
            iclient_controller provides an interface for handling application specific requests.
            Classes derived from iclient_controller decide what reply should client recieve 
            based on some configuration.
        */
        class iclient_controller
        {
        public:
            virtual http::server::reply on_search_page_request() = 0;
            
            virtual http::server::reply on_book_search_query(const std::string& query) = 0;
            
            virtual http::server::reply on_book_info(const std::string& isbn) = 0;
            
            virtual http::server::reply on_book_order(const std::string& isbn) = 0;
            
            virtual http::server::reply on_book_add() = 0;
            
            virtual http::server::reply on_book_edit(const std::string& isbn) = 0;
        };
    }
}

#endif /* iclient_controller_hpp */
