//
//  html_generator_test.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include <third_party/catch/catch.hpp>
#include "html_generator.hpp"

using namespace bookstore::server;

SCENARIO("html_generator responsible for providing html content", "[html_generator]")
{
    WHEN("Asked for the search page html")
    {
        std::string search_page_html = html_generator::search_page_content();
        std::string actual_content = "<html><body><form><input type=\"text\" name=\"search_query\"><input type=\"submit\" value=\"find books\"></form></body></html>";
        REQUIRE(search_page_html == actual_content);
    }
    
    WHEN("Asked for book search query content")
    {
        std::string json_data = "{ \"key\":\"value\" }";
        std::string book_search_content = html_generator::book_search_query_content(json_data);
        //std::string actual_content = openfile adn read;
    }
}