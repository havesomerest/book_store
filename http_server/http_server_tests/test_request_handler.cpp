//
//  test_request_handler.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "test_request_handler.hpp"
#include "iclient_controller.hpp"
#include <memory>

using namespace bookstore::server;

namespace http_server_tests
{
    test_request_handler::test_request_handler(std::shared_ptr<iclient_controller> client_controller)
    : bookstore::server::request_handler(client_controller)
    {
        
    }
    
    http::server::reply test_request_handler::handle_request(const http::server::request& req)
    {
        requests_.push_back(req);
        return bookstore::server::request_handler::handle_request(req);
    }
    
    bool test_request_handler::on_search_page_request_was_called() const
    {
        return !requests_.empty();
    }
}