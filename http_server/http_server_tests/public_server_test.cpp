//
//  public_server_test.cpp
//  http_server
//
//  Created by Alexey on 11/10/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include <third_party/catch/catch.hpp>
#include <thread>
#include <regex>
#include <fstream>
#include <iostream>
#include "lib_http_server/server.hpp"
#include "http_server/request_handler.hpp"
#include "http_server/public_server/customer_controller.hpp"
#include "html_generator.hpp"
#include "lib_book_store/mongo_db_gateway.hpp"

#include "lib_book_store/mock_db_gateway.hpp"
#include "test_request_handler.hpp"
#include "mock_client.hpp"

#include "test_helpers.hpp"

using namespace bookstore::server;
using namespace http_server_tests;

SCENARIO("Public server provides customer interface, a readonly view of the system", "[public_server]")
{
    GIVEN("A server with customer configuration and mocked database")
    {
        /*
        std::shared_ptr<mongo_db_gateway> mongo_db_gw =
            std::make_shared<mongo_db_gateway>();
         */
        std::shared_ptr<mock_db_gateway> mock_db_gw =
            std::make_shared<mock_db_gateway>();
        
        std::shared_ptr<customer_controller> customer_ctrl =
            std::make_shared<customer_controller>(mock_db_gw);
        
        std::shared_ptr<test_request_handler> request_handler =
            std::make_shared<test_request_handler>(customer_ctrl);
        
        http::server::server server("0::0", "8080", ".", request_handler);
        /*
            Start server in a separate thread. We're not worried about
            abnormal thread termination because server handles SIGTERM and should
            be able exit normally.
         */
        std::thread server_thread([&server]()
        {
            try
            {
                server.run();
            }
            catch (const std::exception& e)
            {
              std::cout << "unable to start server, " << e.what() << std::endl;
            }
        });
        server_thread.detach();
        
        WHEN("Client sends request for search page")
        {
            mock_client client("localhost", "8080");
            
            std::string response = client.request_search_page();
            
            THEN("Request handler should recieve onSearchPageRequest()")
            {
                REQUIRE(request_handler->on_search_page_request_was_called());
                REQUIRE(response == correct_search_page_response());
            }
            
            AND_WHEN("Client searches for 'hawking'")
            {
                std::string search_result = client.search_for("awking");
                
                THEN("List of Stephen's books should be displayed")
                {
                    std::string recieved_json = extract_book_info_json(search_result);
                    REQUIRE(recieved_json == correct_search_results_for_hawking());
                }
            }
            
            AND_WHEN("Client wants to get information about Brief History of Time")
            {
                std::string search_result = client.get_book_info("b9b125e6-0594-4740-bbb9-b07657fed7f7");
                
                THEN("Page with detailed information about the book should be displayed")
                {
                    std::string recieved_json = extract_book_info_json(search_result);
                    REQUIRE(recieved_json == correct_info_about_brief_history_of_time());
                }
            }
            
            AND_WHEN("Client wants to order Brief History of Time")
            {
                std::string search_result = client.order_book("b9b125e6-0594-4740-bbb9-b07657fed7f7");
                
                THEN("Order confirmation page should be displayed")
                {
                    std::string recieved_json = extract_book_info_json(search_result);
                    REQUIRE(recieved_json == correct_info_about_brief_history_of_time());
                }
            }
            
            AND_WHEN("Client wants to add book what only admins can do")
            {
                std::string search_result = client.add_book();
                
                THEN("Unathorized message should be displayed")
                {
                    REQUIRE(search_result == unathorized_message());
                }
            }
            
            AND_WHEN("Client wants to edit book what only admins can do")
            {
                std::string search_result = client.edit_book("b9b125e6-0594-4740-bbb9-b07657fed7f7");
                
                THEN("Unathorized message should be displayed")
                {
                    REQUIRE(search_result == unathorized_message());
                }
            }
        }
    }
}