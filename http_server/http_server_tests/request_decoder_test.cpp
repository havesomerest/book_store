//
//  request_decoder_test.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include <third_party/catch/catch.hpp>
#include "request_decoder.hpp"

using namespace bookstore::server;

static http::server::request create_search_page_request();
static http::server::request create_books_query_request();
static http::server::request create_book_info_request();
static http::server::request create_order_book_request();
static http::server::request create_add_book_request();
static http::server::request create_edit_book_request();

SCENARIO("Request decoder should map http requests to a set of actions", "[request_decoder]")
{
    WHEN("Asking decoder for the type of the search page request")
    {
        auto rtype = request_decoder::decode(create_search_page_request());
        THEN("Decoded type should be get_search_page")
        {
            REQUIRE(rtype == client_request_type::get_search_page);
        }
    }
        
    WHEN("Asking decoder for the type of the search books query request")
    {
        auto rtype = request_decoder::decode(create_books_query_request());
        THEN("Decoded type should be get_books_query")
        {
            REQUIRE(rtype == client_request_type::get_books_query);
        }
    }
    
    WHEN("Asking decoder for the type of the book info request")
    {
        auto rtype = request_decoder::decode(create_book_info_request());
        THEN("Decoded type should be get_books_info")
        {
            REQUIRE(rtype == client_request_type::get_book_info);
        }
    }
    
    WHEN("Asking decoder for the type of the order book request")
    {
        auto rtype = request_decoder::decode(create_order_book_request());
        THEN("Decoded type should be order_book")
        {
            REQUIRE(rtype == client_request_type::order_book);
        }
    }
    
    WHEN("Asking decoder for the type of the 'add book' request")
    {
        auto rtype = request_decoder::decode(create_add_book_request());
        THEN("Decoded type should be add_book")
        {
            REQUIRE(rtype == client_request_type::add_book);
        }
    }
    
    WHEN("Asking decoder for the type of the 'edit book' request")
    {
        auto rtype = request_decoder::decode(create_edit_book_request());
        THEN("Decoded type should be edit_book")
        {
            REQUIRE(rtype == client_request_type::edit_book);
        }
    }
    
    WHEN("Asking decoder to extract query /?search_query=neil+degrasse+tyson")
    {
        std::string norm_query = request_decoder::extract_query("/?search_query=neil+degrasse+tyson");
        THEN("Extracted query should be 'neil degrasse tyson'")
        {
            REQUIRE(norm_query == "neil degrasse tyson");
        }
    }
    
    WHEN("Asking decoder to extract isbn /?isbn=0a1d7a41-1d9e-4e68-a7ed-6720091541fci")
    {
        std::string isbn = request_decoder::extract_isbn("/?isbn=0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        THEN("Extracted isbn should be 0a1d7a41-1d9e-4e68-a7ed-6720091541fc")
        {
            REQUIRE(isbn == "0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        }
    }
    
    WHEN("Asking decoder to extract order isbn /?order=0a1d7a41-1d9e-4e68-a7ed-6720091541fci")
    {
        std::string isbn = request_decoder::extract_order_isbn("/?order=0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        THEN("Extracted isbn should be 0a1d7a41-1d9e-4e68-a7ed-6720091541fc")
        {
            REQUIRE(isbn == "0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        }
    }
    
    WHEN("Asking decoder to extract edit isbn /?edit_book=0a1d7a41-1d9e-4e68-a7ed-6720091541fci")
    {
        std::string isbn = request_decoder::extract_edit_isbn("/?edit_book=0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        THEN("Extracted isbn should be 0a1d7a41-1d9e-4e68-a7ed-6720091541fc")
        {
            REQUIRE(isbn == "0a1d7a41-1d9e-4e68-a7ed-6720091541fc");
        }
    }
}

http::server::request create_search_page_request()
{
    http::server::request req { "GET", "/" };
    return std::move(req);
}

http::server::request create_books_query_request()
{
    http::server::request req { "GET", "/?search_query=neil+degrasse+tyson" };
    return std::move(req);
}

http::server::request create_book_info_request()
{
    http::server::request req { "GET", "/?isbn=0a1d7a41-1d9e-4e68-a7ed-6720091541fc" };
    return std::move(req);
}

http::server::request create_order_book_request()
{
    http::server::request req { "GET", "/?order=0a1d7a41-1d9e-4e68-a7ed-6720091541fc" };
    return std::move(req);
}

http::server::request create_add_book_request()
{
    http::server::request req { "GET", "/add_book" };
    return std::move(req);
}

http::server::request create_edit_book_request()
{
    http::server::request req { "GET", "/?edit_book=0a1d7a41-1d9e-4e68-a7ed-6720091541fc" };
    return std::move(req);
}