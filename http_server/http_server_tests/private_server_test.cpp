//
//  private_server_test.cpp
//  http_server
//
//  Created by Alexey on 11/10/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include <third_party/catch/catch.hpp>
#include <thread>
#include <regex>
#include <fstream>
#include <iostream>
#include "lib_http_server/server.hpp"
#include "http_server/request_handler.hpp"
#include "http_server/private_server/admin_controller.hpp"
#include "html_generator.hpp"
#include "lib_book_store/mongo_db_gateway.hpp"

#include "lib_book_store/mock_db_gateway.hpp"
#include "test_request_handler.hpp"
#include "mock_client.hpp"

#include "test_helpers.hpp"

using namespace bookstore::server;
using namespace http_server_tests;

SCENARIO("Private server provides 'admin' view of the system, allowing modifications", "[private_server]")
{
    GIVEN("A server with admin controller and mocked database")
    {
        /*
        std::shared_ptr<mongo_db_gateway> mongo_db_gw =
            std::make_shared<mongo_db_gateway>();
         */
        std::shared_ptr<mock_db_gateway> mock_db_gw =
            std::make_shared<mock_db_gateway>();
        
        std::shared_ptr<admin_controller> admin_ctrl =
            std::make_shared<admin_controller>(mock_db_gw);
        
        std::shared_ptr<test_request_handler> request_handler =
            std::make_shared<test_request_handler>(admin_ctrl);
        
        
        /*
            Start server in a separate thread. We're not worried about
            abnormal thread termination because server handles SIGTERM and should
            be able exit normally.
         */
        http::server::server server("0::0", "8081", ".", request_handler);
        std::thread server_thread([&server]()
        {
            try
            {
                server.run();
            }
            catch (const std::exception& e)
            {
              std::cout << "unable to start server, " << e.what() << std::endl;
            }
        });
        server_thread.detach();
        
        WHEN("Admin sends request for search page")
        {
            // connect to server
            mock_client client("localhost", "8081");
            
            std::string response = client.request_search_page();
            
            THEN("Search page should be displayed as well as 'add' button")
            {
                REQUIRE(request_handler->on_search_page_request_was_called());
                REQUIRE(response == correct_admin_search_page_response());
            }
            
            AND_WHEN("Admin searches for 'hawking'")
            {
                std::string search_result = client.search_for("hawking");
                
                THEN("List of Stephen's books should be displayed")
                {
                    std::string recieved_json = extract_book_info_json(search_result);
                    REQUIRE(recieved_json == correct_search_results_for_hawking());
                }
            }
            
            AND_WHEN("Admin wants to get information about Brief History of Time")
            {
                std::string search_result = client.get_book_info("b9b125e6-0594-4740-bbb9-b07657fed7f7");
                
                THEN("Page with detailed information about the book and edit button should be displayed")
                {
                    std::string recieved_json = extract_book_info_json(search_result);
                    REQUIRE(recieved_json == correct_info_about_brief_history_of_time());
                }
            }
            
            AND_WHEN("Admins wants to edit Brief History of Time")
            {
                std::string search_result = client.edit_book("b9b125e6-0594-4740-bbb9-b07657fed7f7");
                
                THEN("Book info editing page should be displayed")
                {
                    REQUIRE(search_result == book_edit_page_contents());
                }
            }
        }
    }
}
/*
std::string correct_search_page_response()
{
    return html_generator::admin_search_page_content();
}

std::string correct_search_results_for_hawking()
{
    std::ifstream file("test_hawking_results");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}

std::string correct_info_about_brief_history_of_time()
{
    std::ifstream file("brief_history_of_time_info");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}

std::string extract_book_info_json(const std::string& html)
{
    auto str_replace = [](std::string& str, const std::string& from, const std::string& to)
    {
        size_t start_pos = str.find(from);
        while (start_pos != std::string::npos)
        {
            str.replace(start_pos, from.length(), to);
            start_pos = str.find(from);
        }
    };
    // c++11 regex doesn't support multiline match, replace \n with some other symbol
    std::string replaced_html = html;
    str_replace(replaced_html, "\n", "NL");
    
    std::regex rx(".+data-all=\'(.+?)'\\s/>(.*)");
    std::smatch match;
    std::regex_match(replaced_html, match, rx);
    if (match.size() > 1)
        return match[1].str();
    return "";
}

std::string book_edit_page_contents()
{
    std::ifstream file("edit_book_page.html");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}
*/