//
//  test_request_handler.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef test_request_handler_hpp
#define test_request_handler_hpp

#include <vector>
#include "http_server/request_handler.hpp"

namespace bookstore
{
    namespace server
    {
        class iclient_controller;
    }
}

namespace http_server_tests
{
    /*
        Test double for request handler.
        Uses decorator pattern to wrap calls to actual request handler
     */
    class test_request_handler : public bookstore::server::request_handler
    {
    public:
        test_request_handler(std::shared_ptr<bookstore::server::iclient_controller> client_controller);
        
        virtual http::server::reply handle_request(const http::server::request& req) override;
        
        bool on_search_page_request_was_called() const;
        
    private:
        std::vector<http::server::request> requests_;
    };
}




#endif /* test_request_handler_hpp */
