//
//  mock_client.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef mock_client_hpp
#define mock_client_hpp

#include <memory>
#include <string>

namespace http_server_tests {
    
    /*
        mock_client represents controlled client connection, used for tests automation.
        Every function returns html response on a given request.
    */
    class mock_client
    {
    public:
        mock_client(const std::string& addr, const std::string& port);
        ~mock_client();
        
        std::string request_search_page() const;
        
        std::string search_for(const std::string& query) const;
        
        std::string get_book_info(const std::string& isbn) const;
        
        std::string order_book(const std::string& isbn) const;
        
        std::string add_book() const;
        
        std::string edit_book(const std::string& isbn) const;
        
    private:
        std::string addr_;
        std::string port_;
    };
    
}

#endif /* mock_client_hpp */
