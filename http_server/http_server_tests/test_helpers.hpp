//
//  test_helpers.h
//  http_server
//
//  Created by Alexey on 11/12/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef test_helpers_h
#define test_helpers_h

/*
    Private header file providing common functionality for server tests
*/
inline
std::string correct_search_page_response()
{
    return bookstore::server::html_generator::search_page_content();
}

inline
std::string correct_admin_search_page_response()
{
    return bookstore::server::html_generator::admin_search_page_content();
}

inline
std::string correct_search_results_for_hawking()
{
    std::ifstream file("test_hawking_results");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}

inline
std::string correct_info_about_brief_history_of_time()
{
    std::ifstream file("brief_history_of_time_info");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}

inline
std::string extract_book_info_json(const std::string& html)
{
    auto str_replace = [](std::string& str, const std::string& from, const std::string& to)
    {
        size_t start_pos = str.find(from);
        while (start_pos != std::string::npos)
        {
            str.replace(start_pos, from.length(), to);
            start_pos = str.find(from);
        }
    };
    // c++11 regex doesn't support multiline match, replace \n with some other symbol
    std::string replaced_html = html;
    str_replace(replaced_html, "\n", "NL");
    
    std::regex rx(".+data-all=\'(.+?)'\\s/>(.*)");
    std::smatch match;
    std::regex_match(replaced_html, match, rx);
    if (match.size() > 1)
        return match[1].str();
    return "";
}

inline
std::string book_edit_page_contents()
{
    std::ifstream file("edit_book_page.html");
    std::string file_content((std::istreambuf_iterator<char>(file)),
                             std::istreambuf_iterator<char>());
    return file_content;
}

inline
std::string unathorized_message()
{
    return "<html>"
    "<head><title>Unauthorized</title></head>"
    "<body><h1>401 Unauthorized</h1></body>"
    "</html>";
}

#endif /* test_helpers_h */
