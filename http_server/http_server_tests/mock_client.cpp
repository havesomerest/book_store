//
//  mock_client.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "mock_client.hpp"

#include <string>
#include <iostream>
#include <stdio.h>

namespace http_server_tests {
    // quick system call implementation, should be moved in a platform specific header
    std::string exec(const char* cmd) {
        FILE* pipe = popen(cmd, "r");
        if (!pipe) return "ERROR";
        char buffer[128];
        std::string result = "";
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
        pclose(pipe);
        return result;
    }
    
    mock_client::mock_client(const std::string& addr, const std::string& port)
    :
        addr_(addr),
        port_(port)
    {
    }
    
    mock_client::~mock_client()
    {
    }
    
    std::string mock_client::request_search_page() const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/";
        return exec(cmd.c_str());
    }
    
    std::string mock_client::search_for(const std::string& query) const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/?search_query=" + query;
        return exec(cmd.c_str());
    }
    
    std::string mock_client::get_book_info(const std::string &isbn) const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/?isbn=" + isbn;
        return exec(cmd.c_str());
    }
    
    std::string mock_client::order_book(const std::string &isbn) const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/?order=" + isbn;
        return exec(cmd.c_str());
    }
    
    std::string mock_client::add_book() const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/add_book";
        return exec(cmd.c_str());
    }
    
    std::string mock_client::edit_book(const std::string& isbn) const
    {
        std::string cmd = "curl -s " + addr_ + ":" + port_ + "/?edit_book=" + isbn;
        return exec(cmd.c_str());
    }
}