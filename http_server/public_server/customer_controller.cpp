//
//  customer_controller.cpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#include "customer_controller.hpp"
#include "html_generator.hpp"
#include "lib_book_store/idb_gateway.hpp"
#include "request_decoder.hpp"

namespace bookstore
{
    namespace server
    {
        class customer_controller::impl
        {
            std::shared_ptr<idb_gateway> db_gw_;
        public:
            impl(std::shared_ptr<idb_gateway> db_gw)
            {
                db_gw_ = db_gw;
            }
            
            http::server::reply on_search_page_request()
            {
                return http::server::reply(html_generator::search_page_content());
            }
            
            http::server::reply on_book_search_query(const std::string& query)
            {
                std::vector<book_store::book_entry> search_result = db_gw_->find(query);
                
                std::string json_data = book_store::book_entry::to_json(search_result);

                std::string page_content = html_generator::book_search_query_content(json_data);
                
                return http::server::reply(page_content);
            };
            
            http::server::reply on_book_info(const std::string& isbn)
            {
                bool exists{false};
                book_store::book_entry entry{};
                std::tie(exists, entry) = db_gw_->get(isbn);
                
                if (exists)
                {
                    std::string json_data = book_store::book_entry::to_json(entry);
                    std::string page_content = html_generator::book_info_content(json_data);
                    return http::server::reply(page_content);
                }
                else
                    return http::server::reply::stock_reply(http::server::reply::not_found);
            }
            
            http::server::reply on_book_order(const std::string& isbn)
            {
                bool exists{false};
                book_store::book_entry entry{};
                std::tie(exists, entry) = db_gw_->get(isbn);
                
                if (exists)
                {
                    std::string json_data = book_store::book_entry::to_json(entry);
                    std::string page_content = html_generator::book_order_content(json_data);
                    
                    /*
                        This is the place when we can send a mail to the publisher
                        about the order. Currently it is not implemented.
                    */
                    
                    return http::server::reply(page_content);
                }
                else
                    return http::server::reply::stock_reply(http::server::reply::not_found);
            }
            
            http::server::reply on_book_add()
            {
                return http::server::reply::stock_reply(http::server::reply::unauthorized);
            }
            
            http::server::reply on_book_edit(const std::string& isbn)
            {
                return http::server::reply::stock_reply(http::server::reply::unauthorized);
            }
        };
        
        customer_controller::customer_controller(std::shared_ptr<idb_gateway> db_gw)
        {
            impl_ = std::make_shared<impl>(db_gw);
        }
        
        http::server::reply customer_controller::on_search_page_request()
        {
            return impl_->on_search_page_request();
        };
        
        http::server::reply customer_controller::on_book_search_query(const std::string& query)
        {
            return impl_->on_book_search_query(query);
        };
        
        http::server::reply customer_controller::on_book_info(const std::string& isbn)
        {
            return impl_->on_book_info(isbn);
        };
        
        http::server::reply customer_controller::on_book_order(const std::string& isbn)
        {
            return impl_->on_book_order(isbn);
        };
        
        http::server::reply customer_controller::on_book_add()
        {
            return impl_->on_book_add();
        }
        
        http::server::reply customer_controller::on_book_edit(const std::string& isbn)
        {
            return impl_->on_book_edit(isbn);
        };
    }
}