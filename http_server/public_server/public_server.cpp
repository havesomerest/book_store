//
// main.cpp
// ~~~~~~~~

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include "lib_http_server/server.hpp"
#include "request_handler.hpp"
#include "customer_controller.hpp"
#include "lib_book_store/mongo_db_gateway.hpp"
#include "lib_book_store/mock_db_gateway.hpp"

using namespace bookstore::server;

int main(int argc, char* argv[])
{
    try
    {
        // Check command line arguments.
        if (argc != 4)
        {
            std::cerr << "Usage: http_server <address> <port> <doc_root>\n";
            std::cerr << "  For IPv4, try:\n";
            std::cerr << "    receiver 0.0.0.0 80 .\n";
            std::cerr << "  For IPv6, try:\n";
            std::cerr << "    receiver 0::0 80 .\n";
            return 1;
        }
        
        // Initialise the server.
        /*
        std::shared_ptr<mongo_db_gateway> mongo_db_gw =
            std::make_shared<mongo_db_gateway>();
         */
        // Use mocked db for now
        std::shared_ptr<mock_db_gateway> mock_db_gw =
            std::make_shared<mock_db_gateway>();
        
        std::shared_ptr<customer_controller> customer_ctrl =
            std::make_shared<customer_controller>(mock_db_gw);
         
        std::shared_ptr<request_handler> request_hndlr =
            std::make_shared<request_handler>(customer_ctrl);
        
        http::server::server s(argv[1], argv[2], argv[3], request_hndlr);
        
        // Run the server until stopped.
        s.run();
    }
    catch (std::exception& e)
    {
        std::cerr << "exception: " << e.what() << "\n";
    }
    
    return 0;
}