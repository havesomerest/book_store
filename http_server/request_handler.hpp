//
//  request_handler.hpp
//  http_server
//
//  Created by Alexey on 11/11/15.
//  Copyright © 2015 Alexey. All rights reserved.
//

#ifndef request_handler_hpp
#define request_handler_hpp

#include "lib_http_server/irequest_handler.hpp"

namespace bookstore
{
    namespace server
    {
        class iclient_controller;
        
        /*
            request_handler's purpose is to recieve complete http::server::request,
            figure out what kind of request is that and eventually call appropriate
            function on iclient_controller.
        */
        class request_handler : public http::server::irequest_handler
        {
        public:
            request_handler(std::shared_ptr<iclient_controller> client_controller);
            
            virtual http::server::reply handle_request(const http::server::request& req) override;
            
        private:
            class impl;
            std::shared_ptr<impl> impl_;
        };
    }
}

#endif /* request_handler_hpp */
